const express = require('express');
const router = express.Router();
const ctrlLogin = require('./ctrlLogin');

router.post('/', ctrlLogin.login);

module.exports = router;