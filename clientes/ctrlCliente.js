var ctrlCliente = {}

var clientes = [
    {id:1, nome:'Allisson'},
    {id:2, nome:'Jose'},
]


ctrlCliente.list = (req,res)=>{
    res.json(
        clientes
    )
} 

ctrlCliente.add = (req,res)=>{
    let cliente = req.body
    if(Object.keys(cliente).length > 0){
        clientes.push(
            cliente
        )
        res.json(
            {'sucesso':true, 'cliente.add': cliente}
        )
    }else res.sendStatus(400)
}

module.exports = ctrlCliente;