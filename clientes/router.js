const express = require('express');
const router = express.Router();
const ctrlCliente = require('./ctrlCliente')
const private_router = require('../services/verify-header-token')


router.get('/',  ctrlCliente.list)
router.post('/', private_router, ctrlCliente.add)

module.exports = router;